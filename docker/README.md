# joinus deployment

## AWS setup & prerequisites
There is an assumption that you have a standard working setup of docker + aws-cli on the server/workstation you are working on. To configure AWS environment and docker follow:
  - https://docs.docker.com/get-started/
  - https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html

On top of that you are expected to be familiar with SSH-KEY authentication at the basic level.
  - https://www.ssh.com/ssh/key/

## Cloning repository
There is an assumption you know how to work with GIT. If you don't, please follow the tutorial here before you go ahead:
  - https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud

In order to clone the repository, do the following:
```
$ git clone git@bitbucket.org:softwaremill/adam_pokora.git ~/
```
## Local testing
Before you choose to use remote (Bitbucket) pipelining to automatically handle you test process, you have an option to test the deployment locally. In order to test the process locally, we will assume you have the repository cloned in
```
$ ls ~/adam_pokora
-rw-r--r--  1 501  20    625 Oct 16 14:50 Dockerfile
-rw-r--r--  1 501  20   4534 Oct 16 14:50 bitbucket-pipelines.yml
-rw-r--r--  1 501  20  73879 Oct 16 14:50 devops-task.pdf
drwxr-xr-x  7 501  20    224 Oct 16 14:50 relay
-rwxr-xr-x  1 501  20    548 Oct 16 14:50 run.sh
-rw-r--r--  1 501  20    314 Oct 16 14:50 supervisord.conf
drwxr-xr-x  4 501  20    128 Oct 16 16:55 aws
drwxr-xr-x  4 501  20    128 Oct 16 16:55 tests
-rw-r--r--  1 501  20   1840 Oct 16 17:21 logback.xml
drwxr-xr-x  2 501  20     64 Oct 16 17:25 logs
drwxr-xr-x  3 501  20     96 Oct 16 17:26 certs
-rw-r--r--  1 501  20   4180 Oct 16 17:36 README.md
```
Change directory to the cloned repository:

```
$ cd ~/adam_pokora
```

Local test suite will attempt to create a docker image along the same rules as the remote repository setup. The setup wrapper performs local deployment and connection test.

Run local testing suite:
```
$ ./tests/run_local.sh
Initializing local test
Checking local environemnt
Building local package
Sending build context to Docker daemon  249.9kB
Step 1/16 : FROM openjdk:8-jre AS runtime
 ---> 60648c86cfe8
Step 2/16 : ENV MAILHOST="172.17.0.1"
 ---> Using cache
 ---> 73a58b7f99e0
Step 3/16 : RUN mkdir /deploy
 ---> Using cache
 ---> d26d14f7bb3c
Step 4/16 : RUN mkdir /deploy/bin
 ---> Using cache
 ---> e69d36ee4906
Step 5/16 : RUN mkdir /deploy/logs
 ---> Using cache
 ---> 33adb6337fbd
Step 6/16 : RUN mkdir /deploy/etc
 ---> Using cache
 ---> d578f05193e3
Step 7/16 : RUN        true &&            apt-get update &&            apt-get install -y --no-install-recommends ssmtp bsd-mailx supervisor
 ---> Using cache
 ---> ff12f529f138
Step 8/16 : ADD https://s3-eu-west-1.amazonaws.com/softwaremill-public/joinus-devops.jar /deploy/bin/joinus-devops.jar
Downloading [==================================================>]  40.91MB/40.91MB
 ---> Using cache
 ---> f6d719ed0a20
Step 9/16 : COPY logback.xml /deploy/etc/logback.xml
 ---> Using cache
 ---> 5195f64842d4
Step 10/16 : COPY supervisord.conf /etc/supervisord.conf
 ---> Using cache
 ---> b3ba0568f9df
Step 11/16 : COPY run.sh /deploy/bin/run.sh
 ---> Using cache
 ---> 76124aeac8fd
Step 12/16 : RUN  chmod +x /deploy/bin/run.sh
 ---> Using cache
 ---> 62a0bae9225d
Step 13/16 : VOLUME /deploy/logs
 ---> Using cache
 ---> 30990fc00a7e
Step 14/16 : WORKDIR /deploy
 ---> Using cache
 ---> 56f01e4b0813
Step 15/16 : EXPOSE 8081/tcp
 ---> Using cache
 ---> cf3e98010806
Step 16/16 : ENTRYPOINT ["/deploy/bin/run.sh"]
 ---> Using cache
 ---> 5ef4d2dc43ce
Successfully built 5ef4d2dc43ce
Successfully tagged join-us:local
Starting local package
41d23719619e194d59b6b770c9e80ca9e44cc12edafc95a8a684f820db701a83
Testing connectivity...
Successfuly finished.
Cleaning up.
```
## Bootstraping
After cloning the repository it is advisable to create a separate branch and make sure it is your working branch:
```
$ git branch updated-deployment
$ git checkout updated-deployment
$ git branch
master
* updated-deployment
```
Upon checking out new branch, execute:
```
$ aws/bootstrap.sh

Usage: ./aws/bootstrap.sh <ssh-private-key-path> [mail-relay-host]
	<ssh-private-key-path> - required argument pointing to a valid private key
	[mail-relay-host] - optional argument which points to a trusted RELAY HOST for email communication
```
Example command when using RELAY_HOST (also known as the smart host):
 - https://en.wikipedia.org/wiki/Smart_host

```
$ aws/bootstrap.sh ~/.ssh/id_rsa 1.1.1.1
```

Alternative option is to bootstrap servers without trusted RELAY HOST (not recommended, see below):

```
$ aws/bootstrap.sh ~/.ssh/id_rsa
```

The script will prepare two servers:
  - staging
  - production

and configure mail relay agent running postfix inside the container, listening on port 25 on each server.

Bootstrap will also configure remote Amazon ECR repositories for the project and the mail-relay.

Upon completion of bootstrapping process, there will be a list of environment variables you need to setup in your bitbucket repository to take full advantage of pipelining.

example:
```
AWS_ACCESS_KEY_ID=####
AWS_SECRET_ACCESS_KEY=####
AWS_DEFAULT_REGION=eu-west-1
OPS_EMAIL=monitoring@mydomain.com
AWS_REGISTRY_URL=XXXXXXXX.dkr.ecr.eu-west-1.amazonaws.com
PRODUCTION_IP=2.2.2.2
STAGING_IP=3.3.3.3
```
Finally, the bootstrapping process will put necessary credentials to the servers inside the repository in order to deploy CI/CD pipelines at Bitbucket:

```
$ git status
Untracked files:
  (use "git add <file>..." to include in what will be committed)

  certs/production/
  certs/staging/
```
Let's list the directory:
```
$ find certs
certs
certs/staging
certs/staging/id_rsa
certs/staging/key.pem
certs/staging/config.json
certs/staging/server.pem
certs/staging/id_rsa.pub
certs/staging/cert.pem
certs/staging/server-key.pem
certs/staging/ca.pem
certs/.dotfile
certs/production
certs/production/id_rsa
certs/production/key.pem
certs/production/config.json
certs/production/server.pem
certs/production/id_rsa.pub
certs/production/cert.pem
certs/production/server-key.pem
certs/production/ca.pem
```

You should commit and push the branch into remote directory:
```
$ git add -A .
$ git commit -m "Your commit message"
...
$ git push --set-upstream origin updated-deployment
Counting objects: 6, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 1.65 KiB | 1.65 MiB/s, done.
Total 6 (delta 2), reused 0 (delta 0)
remote:
remote: Create pull request for updated-deployment:
remote:   https://bitbucket.org/softwaremill/adam_pokora/pull-requests/new?source=updated-deployment&t=1
remote:
To bitbucket.org:softwaremill/adam_pokora.git
 * [new branch]      updated-deployment -> updated-deployment
Branch 'updated-deployment' set up to track remote branch 'updated-deployment' from 'origin'.
```
Now, since you have successfully pushed your first branch, small explanation regarding the git flow:

When you push your branch into the repository, the pipeline process will kick-in trying to build your commited version with commit tag.

You should develop everything in separate branches and commit what you believe is a working version.

The definition of working version (passed local testing, passed testing with Bitbucket pipelines on commited branch).

If successfully built via bitbucket pipelines, the docker image will end up in ECR repository, properly tagged and you are OK to create a pull request. At the same time assuming you got that far and everything is fine, you should tag your commit with the version of a given release and push it to the repository. Let's say for fun it is *0.0.1*.

```
$ git tag 0.0.1
$ git push --tags
```

Upon merging into master branch, the pipeline will kick-in creating the release with your tag version and docker image will go into staging server with testing validity of deployment.

After successfully deploying image @ staging server, you will have possibility to run production deployment, manually triggering action from the bitbucket.

## Docker image
The image is assumed to run on 8081 port.
branches other than master are tagged with commit hash, master is tagged with git tags. (versions)

## Mail Relay (aka smart host)
Mail relay is configured from within the container of join-us. There is additional container with postfix started on each server (production and staging) which relays all emails either acting as a relay-host or a standalone server. (see README.md inside relay directory).

## Monitoring
I would recommend setting up external monitoring tools such as datadog.

## Extending the image
If you need to add custom configuration to postfix or have it do something outside of the scope of this configuration, simply add your scripts to /docker-init.db/. All files with the .sh extension will be executed automatically at the end of the startup script.

```
FROM join-us
ADD Dockerfiles/additional-config.sh /deploy/docker-init.db/
```

## Comments
The sending email upon exiting the application could be done directly via logback.xml. That was my first attempt, however I realised that there are missing libraries (javax.mail) which i added, and started join-us with modified class path. This however revealed that there are other dependencies missing. I would try to ask development to change the behaviour such that the logback could send emails as well. It's just more elegant.

The configuration of MTA (Mail Transport Agent) is highly dependent on the destination MTA configuration hence it is nearly impossible to assume everything will work out of the box. Normally it is highly recommended to create trusted MTA (or have one) at the company which will be responsible for relaying all of the monitoring communication. All of the necessary options for our dockerized MTA (postfix) to act as a standalone server OR mail-relay agent are present.
