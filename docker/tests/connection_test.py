#!/usr/bin/env python
import requests
import sys
import json

STATUS_OK="ok"
if len(sys.argv) != 2:
    print "Usage: connection_test.py <ip_address>"
    sys.exit(1)

request="http://%s:8081/status" % (sys.argv[1])

try:
    response = requests.get(request,timeout=5)
    status = json.loads(response.text)
    if status['status'] != STATUS_OK:
        print "Failed"
        sys.exit(1)
except Exception,e:
    print e
    sys.exit(1)
sys.exit(0)
