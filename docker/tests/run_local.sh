#!/bin/sh

set -ae

SHELL=/bin/sh
LOCAL_DIR=`pwd`
function log_error
{
    echo $1
    return 1
}
function log_info
{
    echo $1
}

[ ! -x relay ] && log_error "You must execute this from the root directory of your clone."

log_info "Initializing local test"
log_info "Checking local environemnt"
LOG_DIR=${LOCAL_DIR}/logs
[ ! -x ${LOG_DIR} ] && ( mkdir ${LOG_DIR} || log_error "Failed to create \"logs\" directory." )
log_info "Building local package"
docker build -t join-us:local . || log_error "Failed to build local instance."
log_info "Starting local package"
docker run -d --name join-us-local -e MAILHOST=172.17.0.1 -p 8081:8081/tcp -v ${LOG_DIR}:/deploy/logs join-us:local || log_error "Failed to start local instance."
log_info "Testing connectivity..."
sleep 5
python tests/connection_test.py 127.0.0.1 || log_error "Failed the connection test using python..." || log_info "Trying curl..."
/usr/bin/curl -s -X GET localhost:8081/status |grep "ok" > /dev/null || ( docker stop join-us-local > /dev/null && log_error "Curl connection failed." )
log_info "Successfuly finished."
log_info "Cleaning up."
docker stop join-us-local > /dev/null || log_error "Failed to stop docker image: join-us-local"
docker rm join-us-local > /dev/null || log_error "Failed to remove docker image: join-us-local"
