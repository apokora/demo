#!/bin/sh

echo "POSTFIX STARTING UP"

mkdir -p /var/spool/postfix/ && mkdir -p /var/spool/postfix/pid
chown root: /var/spool/postfix/
chown root: /var/spool/postfix/pid

postconf -e smtputf8_enable=no

postconf -e mynetworks_style=subnet
postalias /etc/postfix/aliases

postconf -e mydestination=
postconf -e relay_domains=
postconf -e "message_size_limit=0"
postconf -e smtpd_delay_reject=yes
postconf -e smtpd_helo_required=yes
postconf -e "smtpd_helo_restrictions=permit_mynetworks,reject_invalid_helo_hostname,permit"

if [ -n "$HOSTNAME" ]; then
	postconf -e myhostname="$HOSTNAME"
else
	postconf -# myhostname
fi

if [ -n "$RELAYHOST" ]; then
	echo -n "- Forwarding all emails to $RELAYHOST"
	postconf -e relayhost=$RELAYHOST

	if [ -n "$RELAYHOST_USERNAME" ] && [ -n "$RELAYHOST_PASSWORD" ]; then
		echo " using username $RELAYHOST_USERNAME."
		echo "$RELAYHOST $RELAYHOST_USERNAME:$RELAYHOST_PASSWORD" >> /etc/postfix/sasl_passwd
		postmap hash:/etc/postfix/sasl_passwd
		postconf -e "smtp_sasl_auth_enable=yes"
		postconf -e "smtp_sasl_password_maps=hash:/etc/postfix/sasl_passwd"
		postconf -e "smtp_sasl_security_options=noanonymous"
	else
		echo " without any authentication. Make sure your server is configured to accept emails coming from this IP."
	fi
else
	echo "- Will try to deliver emails directly to the final server. Make sure your DNS is setup properly!"
	postconf -# relayhost
	postconf -# smtp_sasl_auth_enable
	postconf -# smtp_sasl_password_maps
	postconf -# smtp_sasl_security_options
fi
if [ -n "$MYNETWORKS" ]; then
	postconf -e mynetworks=$MYNETWORKS
else
	postconf -e "mynetworks=127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16"
fi

postconf -# "smtpd_restriction_classes"
postconf -e "smtpd_recipient_restrictions=permit_mynetworks,reject_non_fqdn_recipient, reject_unknown_recipient_domain, defer_unauth_destination, permit"

postconf -e "smtpd_relay_restrictions=permit"

if [ -d /docker-init.db/ ]; then
	echo "- Executing any found custom scripts..."
	for f in /docker-init.db/*; do
		case "$f" in
			*.sh)     chmod +x "$f"; echo -e"\trunning $f"; . "$f" ;;
			*)        echo "$0: ignoring $f" ;;
		esac
	done
fi
echo "- Staring rsyslog and postfix"
exec supervisord -c /etc/supervisord.conf
