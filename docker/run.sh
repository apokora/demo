#!/bin/sh

echo ".. :: JOINUS STARTING UP :: .."

echo "hostname=join-us-container" > /etc/ssmtp/ssmtp.conf
echo "root=postmaster" >> /etc/ssmtp/ssmtp.conf
echo "mailhub=$MAILHOST"  >> /etc/ssmtp/ssmtp.conf

if [ -d /deploy/docker-init.db/ ]; then
	echo "- Executing any found custom scripts..."
	for f in /deploy/docker-init.db/*; do
		case "$f" in
			*.sh)     chmod +x "$f"; echo -e"\trunning $f"; . "$f" ;;
			*)        echo "$0: ignoring $f" ;;
		esac
	done
fi

echo "- Staring join-us application -"
exec supervisord -c /etc/supervisord.conf
