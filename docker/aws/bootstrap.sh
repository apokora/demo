#!/bin/sh

set -ae

SHELL=/bin/sh

TEMPLATE_CONFIG=aws/ssmtp.conf
DOCKER_MACHINE_CERTS_PATH=~/.docker/machine/machines

function usage
{
  echo "Usage: $0 <ssh-private-key-path> [mail-relay-host]"
  echo "\t<ssh-private-key-path> - required argument pointing to a valid private key"
  echo "\t[mail-relay-host] - optional arguemnt, which points to a trusted RELAY HOST for email communication"
}

function log_error
{
    echo $1
    return 1
}
function log_info
{
    echo $1
}

function check_if_exists
{
  test -r ${1} || log_error "Failed to find ${1}"
}

function create_default_vpc
{
  aws ec2 create-default-vpc || log_error "Failed to create default VPC!"
}
function check_if_vpc_exists
{
  aws ec2 describe-vpcs | grep default > /dev/null
}
function create_machine
{
  log_info "Creating $1 server with ssh key: $2 ..."
  docker-machine inspect ${1} 2>/dev/null || docker-machine create --driver amazonec2 --amazonec2-tags role,${1} --amazonec2-ssh-keypath ${2} --amazonec2-open-port 8081 --amazonec2-monitoring ${1}  || log_error "Failed to create $1 server."
  log_info "Created $1 server."
}
function install_mail_relay
{
  log_info "Installing mail-relay @ $1 server..."
  SSMTP_CONF=${1}_${TEMPLATE_CONFIG}
  docker-machine ssh ${1} sudo apt-get install --no-install-recommends -y ssmtp bsd-mailx || log_error "Failed to install ssmtp-bsd-mailx @ ${1}"
  [ -r ${SSMTP_CONF} ] || sed "s/__HOSTNAME__/${1}/g" ${TEMPLATE_CONFIG} > ${SSMTP_CONF}
  docker-machine scp ${SSMTP_CONF} ${1}: || log_error "Failed to copy ${SSMTP_CONF} to ${1}"
  docker-machine ssh ${1} sudo mv ${SSMTP_CONF} /etc/ssmtp/ssmtp.conf || log_error "Failed to move ${SSMTP_CONF} to /etc/ssmtp/ssmtp.conf @ ${1}"
  log_info "Finished installing mail-relay @ $1 server."
}
function create_ecr_repository
{
  log_info "Installing $1 reposiotory @ ECR..."
  REPO_PATH=`aws ecr describe-repositories | grep ${1} | awk '{print($6)}'`
  if [ -z "${REPO_PATH}" ]; then
    REPO_PATH=`aws ecr create-repository --repository-name ${1} | grep ${1} | awk '{print($6)}'`
  fi
  if [ -z "${REPO_PATH}" ]; then
    log_error "Failed to get ECR reposiotory name"
  fi
  log_info "Done installing $1 reposiotory @ ECR."
}
function copy_credentials
{
  cp -vrf ${DOCKER_MACHINE_CERTS_PATH}/${1} certs/
}
function build_mail_relay
{
  log_info "Building mail relay docker package: ${REPO_PATH}:latest"
  docker build -t ${REPO_PATH}:latest relay || log_error "Failed to build mail_relay"
  eval $(aws ecr get-login --no-include-email)
  docker push ${REPO_PATH}:latest || log_error "Failed to push mail_relay to ECR"
  log_info "Finished building mail relay docker package."
}
function deploy_mail_relay
{
  eval $(docker-machine env ${1})
  docker run --rm --name postfix -d -e MYNETWORKS=172.17.0.0/16,127.0.0.1 -e HOSTNAME=postfix-docker -e RELAYHOST=${2} -p 25:25 ${REPO_PATH}:latest 2>/dev/null || log_info "Mail relay seems to be running @ ${1}. Skipping."
}
#
[ -z ${1} ] && usage && log_error "Missing ssh-private-key-path argument."
#
check_if_exists ${1} || log_error "Make sure you point out to real private key"
check_if_exists ./aws || log_error "Make sure you execute this script from top level of repository ./aws/bootstrap.sh <ssh-private-key-path> [mail-relay-host]"
#
log_info "Bootstrapping the environment..."
#
log_info "Checking validity of VPC..."
check_if_vpc_exists || ( create_default_vpc && log_info "Created default VPC." )
[ -n "${RELAYHOST}" ] && log_info "Using RELAYHOST=${2}"
#
create_machine staging $1
sleep 1
copy_credentials staging
create_machine production $1
sleep 1
copy_credentials production
#
create_ecr_repository join-us
create_ecr_repository mail-relay
#
build_mail_relay
deploy_mail_relay production ${RELAYHOST}
deploy_mail_relay staging ${RELAYHOST}
#
log_info "Bootstrapping completed."
#
log_info "Export following variables @ your bitbucket repository"
log_info "AWS_ACCESS_KEY_ID=`cat ~/.aws/credentials | grep access_key_id | awk '{print($3)}'`"
log_info "AWS_SECRET_ACCESS_KEY=`cat ~/.aws/credentials | grep secret_access_key | awk '{print($3)}'`"
log_info "AWS_DEFAULT_REGION=`cat ~/.aws/config | grep region | awk '{print($3)}'`"
log_info "OPS_EMAIL=eu.ops@scsociety.com"
log_info "AWS_REGISTRY_URL=`echo ${REPO_PATH} | awk -F/ '{print($1)}'`"
log_info "PRODUCTION_IP=`docker-machine ls |grep production.*tcp | awk '{print($5)}' | awk -F : '{print($2)}' | sed -e 's/\/\///g'`"
log_info "STAGING_IP=`docker-machine ls |grep staging.*tcp | awk '{print($5)}' | awk -F : '{print($2)}' | sed -e 's/\/\///g'`"
