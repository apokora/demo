# POSTFIX relay

Simple postfix relay host for your Docker containers. Based on Alpine Linux

This image allows you to run POSTFIX internally inside your docker cloud/swarm installation to centralise outgoing email sending. The embedded postfix enables you to either send messages directly or relay them to your company's main server.

This is a server side POSTFIX image, geared towards emails that need to be sent from your applications. That's why this postfix configuration does not support username / password login or similar client-side security features.

If you want to set up and manage your POSTFIX installation for end users, this image is not for you. If you need it to manage your application's outgoing queue, read on.


## Configuration

Postfix(MTA) is running on default 25 port.
There are number of environment variables which can be overridden upon starting the docker image.
```
ENV vars
$HOSTNAME = Postfix myhostname
$RELAYHOST = Host that relays your msgs (optional)
$RELAYHOST_USERNAME = An (optional) username for the relay server
$RELAYHOST_PASSWORD = An (optional) login password for the relay server
$MYNETWORKS = allow domains from per Network ( default 127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16 )
```

## Extending the image
If you need to add custom configuration to postfix or have it do something outside of the scope of this configuration, simply add your scripts to /docker-init.db/. All files with the .sh extension will be executed automatically at the end of the startup script.

```
FROM mail-relay
ADD Dockerfiles/additional-config.sh /docker-init.db/
```
